//
//  ViewController.h
//  Ball
//
//  Created by Azizi on 04/04/2019.
//  Copyright © 2019 Aleksander Azizi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>     // Accelerometer
#import <AudioToolbox/AudioToolbox.h> // Audio

@interface ViewController : UIViewController {
    // UI
    CGPoint center;
    
    float circleRadius;
    CGPoint circleAngle;
    CAShapeLayer *circle;
    
    CMMotionManager *motion;
    NSTimer *refresh;
    CMAcceleration movement;
    double sensitivity;
    
    double score;
    UILabel *scoreLabel;
}

@end
