//
//  ViewController.m
//  Ball
//
//  Created by Azizi on 04/04/2019.
//  Copyright © 2019 Aleksander Azizi. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

// Refresh rate
static const NSTimeInterval rate = (1.0 / 60.0); // 60 Hz

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    // Init UI
    [self createMainView];
    [self createCircle];
    
    // Load preferences
    [self loadPreferences];
    
    // Init motion manager
    [self createMotion];
}

- (void)createMainView {
    CGSize viewSize = self.view.frame.size;
    center = CGPointMake(viewSize.width / 2, viewSize.height / 2);
    
    [self.view setBackgroundColor:[UIColor colorWithRed:50.0/255.0 green:240.0/255.0 blue:255.0/255.0 alpha:1.0]];

    // Init grid
    CAShapeLayer *xGridLayer = [CAShapeLayer layer];
    [xGridLayer setPath:[[UIBezierPath bezierPathWithRect:CGRectMake(center.x - 0.5, 0, 1, viewSize.height)] CGPath]];
    [xGridLayer setFillColor:[[UIColor redColor] CGColor]];
    [[self.view layer] addSublayer:xGridLayer];
    
    CAShapeLayer *yGridLayer = [CAShapeLayer layer];
    [yGridLayer setPath:[[UIBezierPath bezierPathWithRect:CGRectMake(0, center.y - 0.5, viewSize.width, 1)] CGPath]];
    [yGridLayer setFillColor:[[UIColor redColor] CGColor]];
    [[self.view layer] addSublayer:yGridLayer];
    
    
    // Init score
    CGRect scoreLabelRect = CGRectMake(center.x - (200 / 2), 50, 200, 80);

    scoreLabel = [[UILabel alloc] initWithFrame:scoreLabelRect];
    [scoreLabel setTextColor:[UIColor whiteColor]];
    [scoreLabel setTextAlignment:NSTextAlignmentCenter];
    [scoreLabel setFont:[UIFont systemFontOfSize:42.0f weight:UIFontWeightBold]];
    
    [self.view addSubview:scoreLabel];
    
    // Reset score on tap
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resetScore)];
    [tapGesture setNumberOfTapsRequired:1];
    [self.view setUserInteractionEnabled:true];
    [self.view addGestureRecognizer:tapGesture];
}

- (void)createCircle {
    circleRadius = 30.0f;
    circleAngle = CGPointMake((-M_PI/2), (3 * M_PI/2));
    
    circle = [CAShapeLayer layer];
    [circle setPath:[[UIBezierPath bezierPathWithArcCenter:center radius:circleRadius startAngle:circleAngle.x endAngle:circleAngle.y clockwise:true] CGPath]];
    [circle setFillColor:[[UIColor whiteColor] CGColor]];
    
    [[self.view layer] addSublayer:circle];
}

- (void)loadPreferences {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults synchronize];
    
    NSInteger sensitivityValue = [defaults integerForKey:@"sensitivity"];
    if (!sensitivityValue) {
        sensitivity = 2 * 0.1; // Default
    } else {
        sensitivity = (long)sensitivityValue * 0.1;
    }
}

- (void)createMotion {
    motion = [[CMMotionManager alloc] init];
    
    // Make sure the accelerometer hardware is available (only avalible on real device).
    if (motion.isAccelerometerAvailable) {
        [motion setAccelerometerUpdateInterval:rate];
        [motion startAccelerometerUpdates];
        
        // Configure refresh timer to fetch the data.
        refresh = [NSTimer scheduledTimerWithTimeInterval:rate target:self selector:@selector(move) userInfo:nil repeats:true];
    }
}

// Move circle (x, y)
- (void)move {
    CGPoint circlePoint = circle.position;
    
    circlePoint.x = circlePoint.x + motion.accelerometerData.acceleration.x * 15;
    circlePoint.y = circlePoint.y - motion.accelerometerData.acceleration.y * 15;
    
    [circle setPosition:circlePoint];
    
    if (motion.accelerometerData.acceleration.z > sensitivity) {
        movement = motion.accelerometerData.acceleration;
        if (movement.z > score) [self setScore:movement.z];
        [self moveThrow];
    }
    
    //NSLog(@"x: %f | y: %f | z: %f", motion.accelerometerData.acceleration.x, motion.accelerometerData.acceleration.y, motion.accelerometerData.acceleration.z);
}

// Move circle (z)
- (void)moveThrow {
    double force = movement.z * 3;
    
    int growRadius = (circleRadius * force),
        shrinkRadius = circleRadius;
    
    // Init animation for throw
    UIBezierPath *growPath   = [UIBezierPath bezierPathWithArcCenter:CGPointMake(growRadius,   growRadius)   radius:growRadius   startAngle:circleAngle.x endAngle:circleAngle.y clockwise:true],
                 *shrinkPath = [UIBezierPath bezierPathWithArcCenter:CGPointMake(shrinkRadius, shrinkRadius) radius:shrinkRadius startAngle:circleAngle.x endAngle:circleAngle.y clockwise:true];
    
    CGPoint circlePoint = CGPointMake(circle.position.x - center.x, circle.position.y - center.y);
    CGRect  growBounds   = CGRectMake(circlePoint.x, circlePoint.y, growRadius   * 2, growRadius   * 2),
            shrinkBounds = CGRectMake(circlePoint.x, circlePoint.y, shrinkRadius * 2, shrinkRadius * 2);
    
    
    CABasicAnimation *growPathAnimation = [CABasicAnimation animationWithKeyPath:@"path"],
                     *shrinkPathAnimation = [CABasicAnimation animationWithKeyPath:@"path"],
                     *growBoundsAnimation = [CABasicAnimation animationWithKeyPath:@"bounds"],
                     *shrinkBoundsAnimation = [CABasicAnimation animationWithKeyPath: @"bounds"];
    
    [growPathAnimation     setToValue:(id)growPath.CGPath];
    [shrinkPathAnimation   setToValue:(id)shrinkPath.CGPath];
    [growBoundsAnimation   setToValue:[NSValue valueWithCGRect:growBounds]];
    [shrinkBoundsAnimation setToValue:[NSValue valueWithCGRect:shrinkBounds]];
    
    NSArray *animations = [NSArray arrayWithObjects:growPathAnimation, growBoundsAnimation, shrinkPathAnimation, shrinkBoundsAnimation, nil];
    CAAnimationGroup *throwAnimation = [CAAnimationGroup animation];
    
    [throwAnimation setAnimations:animations];
    [throwAnimation setDuration:(force * 0.2)];
    [throwAnimation setRemovedOnCompletion:false];
    [throwAnimation setFillMode:kCAFillModeForwards];
    
    [circle addAnimation:throwAnimation forKey:nil];
}

- (void)setScore:(double)value {
    score = value;
    
    float multiplier = circleRadius * 3;
    NSString *scoreText = [NSString stringWithFormat:@"%.0f", value * multiplier];
    [scoreLabel setText:scoreText];
    
    // Sound
    AudioServicesPlaySystemSound(1323);
    
    // Score font increase
    [self->scoreLabel setFont:[UIFont systemFontOfSize:80.0f weight:UIFontWeightBold]];
    [NSTimer scheduledTimerWithTimeInterval:1.0f target:[NSBlockOperation blockOperationWithBlock:^{
        [self->scoreLabel setFont:[UIFont systemFontOfSize:42.0f weight:UIFontWeightBold]];
    }] selector:@selector(main) userInfo:nil repeats:false ];
}

- (void)resetScore {
    score = 0;
    [scoreLabel setText:@"0"];
}

@end
